
FROM debian:latest
RUN apt-get update
RUN apt-get install -y cowsay fortune
ENTRYPOINT ["/usr/games/cowsay"]
COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"] 
